use gtk::glib;
use gtk::prelude::*;
use gtk::MessageDialog;
use gtk::{
    AccelFlags, AccelGroup, Application, ApplicationWindow, Label, Menu, MenuBar, MenuItem, Window,
    WindowPosition,
};
use smartsync_core::{config, registry, Config, FileSync};

const DEVICE: &str = "device: ";
const NAME: &str = "name: ";
const SOURCES: &str = "sources:";
const DEST: &str = "dest: ";

fn main() {
    let app = Application::builder()
        .application_id("org.miller-time.SmartSync")
        .build();

    app.connect_activate(build_ui);

    app.run();
}

fn build_ui(app: &Application) {
    let accel_group = AccelGroup::new();

    let win = ApplicationWindow::builder()
        .application(app)
        .default_width(1024)
        .default_height(768)
        .window_position(WindowPosition::Center)
        .title("Smart Sync")
        .build();

    win.add_accel_group(&accel_group);

    let v_box = gtk::Box::new(gtk::Orientation::Vertical, 10);
    let menu_bar = build_menubar(&app, &win, &accel_group);
    v_box.add(&menu_bar);
    win.add(&v_box);

    win.show_all();
}

fn build_menubar(app: &Application, win: &ApplicationWindow, accel_group: &AccelGroup) -> MenuBar {
    let menu_bar = MenuBar::new();

    build_file_menu(&menu_bar, win, accel_group);
    build_config_menu(&menu_bar, app);

    menu_bar
}

fn build_file_menu(menu_bar: &MenuBar, win: &ApplicationWindow, accel_group: &AccelGroup) {
    let file = MenuItem::with_label("File");

    let menu = Menu::new();

    let quit = MenuItem::with_label("Quit");
    quit.connect_activate(glib::clone!(@weak win => move |_| {
        win.close();
    }));
    let (key, modifier) = gtk::accelerator_parse("<Primary>Q");
    quit.add_accelerator("activate", accel_group, key, modifier, AccelFlags::VISIBLE);
    menu.append(&quit);

    file.set_submenu(Some(&menu));

    menu_bar.append(&file);
}

fn build_config_menu(menu_bar: &MenuBar, app: &Application) {
    let config = MenuItem::with_label("Config");

    let menu = Menu::new();

    let configure = MenuItem::with_label("Configure Backups");
    configure.connect_activate(glib::clone!(@weak app => move |_| {
        open_config_window(&app);
    }));
    menu.append(&configure);

    config.set_submenu(Some(&menu));

    menu_bar.append(&config);
}

fn open_config_window(app: &Application) {
    let window = Window::builder()
        .application(app)
        .default_width(800)
        .default_height(600)
        .window_position(WindowPosition::Center)
        .title("Configuration")
        .build();

    if let Some(backup_list) = build_backup_list() {
        window.add(&backup_list);
    }

    window.show_all();
}

fn build_backup_list() -> Option<gtk::Box> {
    match registry::load_registry() {
        Ok(reg) => {
            let v_box = gtk::Box::new(gtk::Orientation::Vertical, 10);
            for (i, b) in reg.backups.iter().enumerate() {
                if b.path.exists() {
                    if let Ok(config) = config::load_config(&b.path) {
                        let item = build_backup_item(&config);
                        v_box.add(&item);

                        if i + 1 < reg.backups.len() {
                            let sep = gtk::Separator::new(gtk::Orientation::Horizontal);
                            v_box.add(&sep);
                        }
                    }
                } else {
                    let name = format!("{:?} (offline)", b);
                    let name_label = Label::new(Some(&name));
                    v_box.add(&name_label);
                }
            }
            Some(v_box)
        }
        Err(error) => {
            error_dialog(&error.to_string());
            None
        }
    }
}

fn build_backup_item(config: &Config) -> gtk::Box {
    let v_box = gtk::Box::new(gtk::Orientation::Vertical, 10);

    for (i, d) in config.devices.iter().enumerate() {
        let name_box = gtk::Box::new(gtk::Orientation::Horizontal, 10);
        name_box.set_halign(gtk::Align::Center);
        let name_label = Label::new(Some(DEVICE));
        name_box.add(&name_label);
        let name_value = Label::new(Some(&d.name));
        name_box.add(&name_value);
        v_box.add(&name_box);

        for (j, f) in d.files.iter().enumerate() {
            let item = build_sync_item(f);
            v_box.add(&item);

            if j + 1 < d.files.len() {
                let sep = gtk::Separator::new(gtk::Orientation::Horizontal);
                v_box.add(&sep);
            }
        }

        if let Some(last_backup) = d.last_backup {
            let last_backup = format!("{}", last_backup);
            let last_backup_label = Label::new(Some(&last_backup));
            v_box.add(&last_backup_label);
        }

        if i + 1 < config.devices.len() {
            let sep = gtk::Separator::new(gtk::Orientation::Horizontal);
            v_box.add(&sep);
        }
    }

    v_box
}

fn build_sync_item(file_sync: &FileSync) -> gtk::Box {
    let v_box = gtk::Box::new(gtk::Orientation::Vertical, 10);

    let name_box = gtk::Box::new(gtk::Orientation::Horizontal, 10);
    name_box.set_halign(gtk::Align::Center);
    let name_label = Label::new(Some(NAME));
    name_box.add(&name_label);
    let name_value = Label::new(Some(&file_sync.name));
    name_box.add(&name_value);
    v_box.add(&name_box);

    let sources_label = Label::new(Some(SOURCES));
    v_box.add(&sources_label);
    let sources_list = build_sources_list(file_sync);
    v_box.add(&sources_list);

    let dest_box = gtk::Box::new(gtk::Orientation::Horizontal, 10);
    dest_box.set_halign(gtk::Align::Center);
    let dest_label = Label::new(Some(DEST));
    dest_box.add(&dest_label);
    let dest_value = Label::new(Some(&file_sync.dest_str()));
    dest_box.add(&dest_value);
    v_box.add(&dest_box);

    v_box
}

fn build_sources_list(file_sync: &FileSync) -> gtk::TreeView {
    let list = gtk::TreeView::new();
    list.set_halign(gtk::Align::Center);
    list.set_headers_visible(false);
    list.set_hover_selection(true);
    let column = gtk::TreeViewColumn::new();
    let cell = gtk::CellRendererText::new();
    column.pack_start(&cell, true);
    column.add_attribute(&cell, "text", 0);
    list.append_column(&column);

    let model = gtk::ListStore::new(&[String::static_type()]);
    for s in file_sync.sources_str() {
        model.insert_with_values(None, &[(0, &s)]);
    }
    list.set_model(Some(&model));

    list
}

fn error_dialog(message: &str) {
    let dialog = MessageDialog::builder().text(message).build();

    dialog.run();
}
